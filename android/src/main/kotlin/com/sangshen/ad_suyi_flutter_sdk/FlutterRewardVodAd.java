package com.sangshen.ad_suyi_flutter_sdk;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cn.admobiletop.adsuyi.ad.ADSuyiRewardVodAd;
import cn.admobiletop.adsuyi.ad.data.ADSuyiRewardVodAdInfo;
import cn.admobiletop.adsuyi.ad.error.ADSuyiError;
import cn.admobiletop.adsuyi.ad.listener.ADSuyiRewardVodAdListener;
import cn.admobiletop.adsuyi.util.ADSuyiAdUtil;
import cn.admobiletop.adsuyi.util.ADSuyiToastUtil;

public class FlutterRewardVodAd extends FlutterAd.FlutterOverlayAd implements FlutterDestroyableAd {

    @NonNull
    private final AdInstanceManager manager;
    /**
     * 广告位id
     */
    @NonNull
    private final String adPosId;
    /**
     * 场景id可选
     */
    @NonNull
    private final String sceneId;
    /**
     * 广告对象
     */
    private ADSuyiRewardVodAd adSuyiRewardVodAd;
    /**
     * 激励视频对象
     */
    private ADSuyiRewardVodAdInfo _rewardVodAdInfo;

    @Override
    void show() {
        if (_rewardVodAdInfo != null) {
            // TODO 激励视频广告对象一次成功拉取的广告数据只允许展示一次
            ADSuyiAdUtil.showRewardVodAdConvenient(manager.activity, _rewardVodAdInfo);
        }
    }

    static class Builder {
        @Nullable
        private AdInstanceManager manager;
        @Nullable
        private String adPosId;
        @Nullable
        private String sceneId;

        public Builder setManager(@NonNull AdInstanceManager manager) {
            this.manager = manager;
            return this;
        }

        public Builder setAdPosId(@NonNull String adPosId) {
            this.adPosId = adPosId;
            return this;
        }

        public Builder setSceneId(@NonNull String sceneId) {
            this.sceneId = sceneId;
            return this;
        }

        FlutterRewardVodAd build() {
            if (manager == null) {
                throw new IllegalStateException("AdInstanceManager cannot not be null.");
            } else if (adPosId == null) {
                throw new IllegalStateException("adPosId cannot not be null.");
            }

            final FlutterRewardVodAd rewardVodAd = new FlutterRewardVodAd(manager, adPosId, sceneId);
            return rewardVodAd;
        }
    }

    private FlutterRewardVodAd(
            @NonNull AdInstanceManager manager, @NonNull String adPosId, @NonNull String sceneId) {
        this.manager = manager;
        this.adPosId = adPosId;
        this.sceneId = sceneId;
    }

    @Override
    void load() {
        adSuyiRewardVodAd = new ADSuyiRewardVodAd(manager.activity);
        // 设置仅支持的广告平台，设置了这个值，获取广告时只会去获取该平台的广告，null或空字符串为不限制，默认为null，方便调试使用，上线时建议不设置
        adSuyiRewardVodAd.setOnlySupportPlatform(ADSuyiDemoConstant.REWARD_VOD_AD_ONLY_SUPPORT_PLATFORM);
        // 设置激励视频广告监听
        adSuyiRewardVodAd.setListener(new ADSuyiRewardVodAdListener() {
            @Override
            public void onVideoCache(ADSuyiRewardVodAdInfo adSuyiRewardVodAdInfo) {
                // 目前汇量和Inmobi走了该回调之后才准备好
                Log.d(ADSuyiDemoConstant.TAG, "onVideoCache...");
            }

            @Override
            public void onVideoComplete(ADSuyiRewardVodAdInfo adSuyiRewardVodAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onVideoComplete...");
            }

            @Override
            public void onVideoError(ADSuyiRewardVodAdInfo adSuyiRewardVodAdInfo, ADSuyiError adSuyiError) {
                Log.d(ADSuyiDemoConstant.TAG, "onVideoError..." + adSuyiError.toString());
            }

            @Override
            public void onReward(ADSuyiRewardVodAdInfo adSuyiRewardVodAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onReward...");
                manager.onAdReward(FlutterRewardVodAd.this);
            }

            @Override
            public void onAdReceive(ADSuyiRewardVodAdInfo rewardVodAdInfo) {
                _rewardVodAdInfo = rewardVodAdInfo;
                Log.d(ADSuyiDemoConstant.TAG, "onAdReceive...");
                manager.onAdReceive(FlutterRewardVodAd.this);
                ADSuyiToastUtil.show(manager.activity.getApplicationContext(), "激励视频广告获取成功");
            }

            @Override
            public void onAdExpose(ADSuyiRewardVodAdInfo adSuyiRewardVodAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdExpose...");
                manager.onAdExpose(FlutterRewardVodAd.this);
            }

            @Override
            public void onAdClick(ADSuyiRewardVodAdInfo adSuyiRewardVodAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdClick...");
                manager.onAdClick(FlutterRewardVodAd.this);
            }

            @Override
            public void onAdClose(ADSuyiRewardVodAdInfo adSuyiRewardVodAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdClose...");
                manager.onAdClose(FlutterRewardVodAd.this);
            }

            @Override
            public void onAdFailed(ADSuyiError adSuyiError) {
                if (adSuyiError != null) {
                    String failedJosn = adSuyiError.toString();
                    Log.d(ADSuyiDemoConstant.TAG, "onAdFailed..." + failedJosn);
                    manager.onAdFailed(FlutterRewardVodAd.this, adSuyiError);
                }
            }
        });

        // 设置场景id
        adSuyiRewardVodAd.setSceneId(sceneId);
        // 加载广告，参数为广告位ID
        adSuyiRewardVodAd.loadAd(adPosId);
    }

    @Override
    public void release() {
        if (adSuyiRewardVodAd != null) {
            adSuyiRewardVodAd.release();
        }
    }
}
