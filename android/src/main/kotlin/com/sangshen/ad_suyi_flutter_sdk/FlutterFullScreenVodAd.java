package com.sangshen.ad_suyi_flutter_sdk;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cn.admobiletop.adsuyi.ad.ADSuyiFullScreenVodAd;
import cn.admobiletop.adsuyi.ad.ADSuyiRewardVodAd;
import cn.admobiletop.adsuyi.ad.data.ADSuyiFullScreenVodAdInfo;
import cn.admobiletop.adsuyi.ad.error.ADSuyiError;
import cn.admobiletop.adsuyi.ad.listener.ADSuyiFullScreenVodAdListener;
import cn.admobiletop.adsuyi.util.ADSuyiAdUtil;
import cn.admobiletop.adsuyi.util.ADSuyiToastUtil;

public class FlutterFullScreenVodAd extends FlutterAd.FlutterOverlayAd implements FlutterDestroyableAd {

    @NonNull
    private final AdInstanceManager manager;
    /**
     * 广告位id
     */
    @NonNull
    private final String adPosId;
    /**
     * 场景id可选
     */
    @NonNull
    private final String sceneId;
    /**
     * 广告对象
     */
    private ADSuyiFullScreenVodAd adSuyiFullScreenVodAd;
    /**
     * 全屏视频广告对象
     */
    private ADSuyiFullScreenVodAdInfo _fullScreenVodAdInfo;

    @Override
    void show() {
        if (_fullScreenVodAdInfo != null) {
            // TODO 全屏视频广告对象一次成功拉取的广告数据只允许展示一次
            ADSuyiAdUtil.showFullScreenAdConvenient(manager.activity, _fullScreenVodAdInfo);
        }
    }

    static class Builder {
        @Nullable
        private AdInstanceManager manager;
        @Nullable
        private String adPosId;
        @Nullable
        private String sceneId;

        public Builder setManager(@NonNull AdInstanceManager manager) {
            this.manager = manager;
            return this;
        }

        public Builder setAdPosId(@NonNull String adPosId) {
            this.adPosId = adPosId;
            return this;
        }

        public Builder setSceneId(@NonNull String sceneId) {
            this.sceneId = sceneId;
            return this;
        }

        FlutterFullScreenVodAd build() {
            if (manager == null) {
                throw new IllegalStateException("AdInstanceManager cannot not be null.");
            } else if (adPosId == null) {
                throw new IllegalStateException("adPosId cannot not be null.");
            }

            final FlutterFullScreenVodAd rewardVodAd = new FlutterFullScreenVodAd(manager, adPosId, sceneId);
            return rewardVodAd;
        }
    }

    private FlutterFullScreenVodAd(
            @NonNull AdInstanceManager manager, @NonNull String adPosId, @NonNull String sceneId) {
        this.manager = manager;
        this.adPosId = adPosId;
        this.sceneId = sceneId;
    }

    @Override
    void load() {
        adSuyiFullScreenVodAd = new ADSuyiFullScreenVodAd(manager.activity);
        // 设置仅支持的广告平台，设置了这个值，获取广告时只会去获取该平台的广告，null或空字符串为不限制，默认为null，方便调试使用，上线时建议不设置
        adSuyiFullScreenVodAd.setOnlySupportPlatform(ADSuyiDemoConstant.FULL_SCREEN_VOD_AD_ONLY_SUPPORT_PLATFORM);
        // 设置全屏视频监听
        adSuyiFullScreenVodAd.setListener(new ADSuyiFullScreenVodAdListener() {
            @Override
            public void onVideoCache(ADSuyiFullScreenVodAdInfo adSuyiFullScreenVodAdInfo) {
                // 目前汇量和Inmobi走了该回调之后才准备好
                Log.d(ADSuyiDemoConstant.TAG, "onVideoCache...");
            }

            @Override
            public void onVideoComplete(ADSuyiFullScreenVodAdInfo adSuyiFullScreenVodAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onVideoComplete...");
            }

            @Override
            public void onVideoError(ADSuyiFullScreenVodAdInfo adSuyiFullScreenVodAdInfo, ADSuyiError adSuyiError) {
                Log.d(ADSuyiDemoConstant.TAG, "onVideoError..." + adSuyiError.toString());
            }

            @Override
            public void onAdReceive(ADSuyiFullScreenVodAdInfo fullScreenVodAdInfo) {
                _fullScreenVodAdInfo = fullScreenVodAdInfo;
                ADSuyiToastUtil.show(manager.activity.getApplicationContext(), "全屏视频广告获取成功");
                Log.d(ADSuyiDemoConstant.TAG, "onAdReceive...");
                manager.onAdReceive(FlutterFullScreenVodAd.this);
            }

            @Override
            public void onAdExpose(ADSuyiFullScreenVodAdInfo adSuyiFullScreenVodAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdExpose...");
                manager.onAdExpose(FlutterFullScreenVodAd.this);
            }

            @Override
            public void onAdClick(ADSuyiFullScreenVodAdInfo adSuyiFullScreenVodAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdClick...");
                manager.onAdClick(FlutterFullScreenVodAd.this);
            }

            @Override
            public void onAdClose(ADSuyiFullScreenVodAdInfo adSuyiFullScreenVodAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdClose...");
                manager.onAdClose(FlutterFullScreenVodAd.this);
            }

            @Override
            public void onAdFailed(ADSuyiError adSuyiError) {
                // ADSuyiToastUtil.show(getApplicationContext(), "广告获取失败");
                if (adSuyiError != null) {
                    String failedJson = adSuyiError.toString();
                    Log.d(ADSuyiDemoConstant.TAG, "onAdFailed..." + failedJson);
                    manager.onAdFailed(FlutterFullScreenVodAd.this, adSuyiError);
                }
            }
        });

        // 加载广告，参数为广告位ID
        adSuyiFullScreenVodAd.loadAd(adPosId);
    }

    @Override
    public void release() {
        if (adSuyiFullScreenVodAd != null) {
            adSuyiFullScreenVodAd.release();
        }
    }
}
