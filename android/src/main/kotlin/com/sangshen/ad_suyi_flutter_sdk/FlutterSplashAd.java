package com.sangshen.ad_suyi_flutter_sdk;

import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.flutter.plugin.platform.PlatformView;

public class FlutterSplashAd extends FlutterAd implements FlutterDestroyableAd {
    @NonNull
    private final AdInstanceManager manager;
    /**
     * 广告位id
     */
    @NonNull
    private final String adPosId;

    static class Builder {
        @Nullable
        private AdInstanceManager manager;
        @Nullable
        private String adPosId;

        public Builder setManager(@NonNull AdInstanceManager manager) {
            this.manager = manager;
            return this;
        }

        public Builder setAdPosId(@NonNull String adPosId) {
            this.adPosId = adPosId;
            return this;
        }

        FlutterSplashAd build() {
            if (manager == null) {
                throw new IllegalStateException("AdInstanceManager cannot not be null.");
            } else if (adPosId == null) {
                throw new IllegalStateException("adPosId cannot not be null.");
            }

            final FlutterSplashAd bannerAd = new FlutterSplashAd(manager, adPosId);
            return bannerAd;
        }
    }

    private FlutterSplashAd(
            @NonNull AdInstanceManager manager, @NonNull String adPosId) {
        this.manager = manager;
        this.adPosId = adPosId;
    }

    @Override
    void load() {
        SplashAdActivity.startActivity(manager.activity, manager, manager.adIdFor(this), adPosId);
    }

    @Override
    public void release() {

    }
}
