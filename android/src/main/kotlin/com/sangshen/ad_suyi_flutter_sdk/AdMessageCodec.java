package com.sangshen.ad_suyi_flutter_sdk;

import java.io.ByteArrayOutputStream;

import io.flutter.plugin.common.StandardMessageCodec;

public class AdMessageCodec extends StandardMessageCodec {
    private static final byte VALUE_AD_SIZE = (byte) 128;
    private static final byte VALUE_AD_REQUEST = (byte) 129;
    private static final byte VALUE_REWARD_ITEM = (byte) 132;
    private static final byte VALUE_LOAD_AD_ERROR = (byte) 133;
    private static final byte VALUE_PUBLISHER_AD_REQUEST = (byte) 134;
    private static final byte VALUE_INITIALIZATION_STATE = (byte) 135;
    private static final byte VALUE_ADAPTER_STATUS = (byte) 136;
    private static final byte VALUE_INITIALIZATION_STATUS = (byte) 137;

    @Override
    protected void writeValue(ByteArrayOutputStream stream, Object value) {

    }

}
