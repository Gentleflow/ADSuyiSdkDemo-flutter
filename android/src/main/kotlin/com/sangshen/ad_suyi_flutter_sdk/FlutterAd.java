package com.sangshen.ad_suyi_flutter_sdk;

public abstract class FlutterAd {
    abstract static class FlutterOverlayAd extends FlutterAd {
        abstract void show();
    }

    abstract void load();
}
