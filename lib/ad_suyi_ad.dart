import 'ad_suyi_base.dart';
import 'ad_suyi_flutter_sdk.dart';

class ADSuyiSplashAd extends ADSuyiAdLoader {
  String posId;
  String imgName;
  ADSuyiSplashAd({this.posId, this.imgName}) {
    regist();
  }
  void loadAndShow() {
    AdSuyiFlutterSdk.loadSplashAd(
        posId: this.posId, adId: this.adId, imgName: this.imgName);
  }

  void release() {
    unregist();
  }
}

class ADSuyiRewardAd extends ADSuyiAdLoader {
  String posId;
  ADSuyiRewardAd({this.posId}) {
    regist();
  }
  void load() {
    AdSuyiFlutterSdk.loadRewardAd(posId: this.posId, adId: this.adId);
  }

  void show() {
    AdSuyiFlutterSdk.showRewardAd(adId: this.adId);
  }

  void release() {
    unregist();
  }
}

class ADSuyiIntertitialAd extends ADSuyiAdLoader {
  String posId;
  ADSuyiIntertitialAd({this.posId}) {
    regist();
  }
  void load() {
    AdSuyiFlutterSdk.loadIntertitialAd(posId: this.posId, adId: this.adId);
  }

  void show() {
    AdSuyiFlutterSdk.showIntertitialAd(adId: this.adId);
  }

  void release() {
    unregist();
  }
}

class ADSuyiFullScreenVodAd extends ADSuyiAdLoader {
  String posId;
  ADSuyiFullScreenVodAd({this.posId}) {
    regist();
  }
  void load() {
    AdSuyiFlutterSdk.loadFullscreenVodAd(posId: this.posId, adId: this.adId);
  }

  void show() {
    AdSuyiFlutterSdk.showFullscreenVodAd(adId: this.adId);
  }

  void release() {
    unregist();
  }
}

class ADSuyiFlutterBannerAd extends ADSuyiAdView {
  String posId;
  double width;
  double height;
  ADSuyiFlutterBannerAd({this.posId, this.width, this.height}) {
    regist();
  }
  void loadAndShow() {
    AdSuyiFlutterSdk.loadBannerAd(
        posId: this.posId,
        adId: this.adId,
        adWidth: this.width,
        adHeight: this.height);
  }

  void release() {
    unregist();
  }
}

class ADSuyiFlutterNativeAd extends ADSuyiAdLoader {
  String posId;
  double width;
  ADSuyiFlutterNativeAd({this.posId, this.width}) {
    regist();
  }

  void load() {
    AdSuyiFlutterSdk.loadNativeAd(posId: this.posId, width: this.width, adId: this.adId);
  }

  void onReceivedHandle(int adViewId, double width, double height) {
    ADSuyiFlutterNativeAdView adView = ADSuyiFlutterNativeAdView(adViewId);
    adView.height = height;
    adView.width = width;
    if(this.onReceived != null) {
      this.onReceived(adView);
    }
  }

  void release() {
    unregist();
  }
}

class ADSuyiFlutterNativeAdView extends ADSuyiAdView {
  ADSuyiFlutterNativeAdView(int adId) {
    this.adId = adId;
    regist();
  }

  void release() {
    unregist();
  }
}
