import 'dart:collection';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import 'ad_suyi_flutter_sdk.dart';
import 'ad_suyi_ad.dart';

abstract class ADSuyiAd {
  int adId = 0;

  void Function() onSucced;
  void Function() onFailed;
  void Function() onClosed;
  void Function() onClicked;
  void Function() onExposed;
  // rewardAd only
  void Function() onRewarded;
  // nativeAd only
  void Function(ADSuyiFlutterNativeAdView adView) onReceived;

  void regist() {
    adId = instanceManager.regist(this);
  }

  void unregist() {
    AdSuyiFlutterSdk.releaseAd(adId: this.adId);
    instanceManager.unregist(this);
  }
}

class ADSuyiAdView extends ADSuyiAd {
  double width;
  double height;
}

class ADSuyiAdLoader extends ADSuyiAd {}

ADSuyiAdManager instanceManager = ADSuyiAdManager();

class ADSuyiAdManager {
  HashMap<int, dynamic> map = new HashMap();
  int adId = 0;
  int regist(ADSuyiAd ad) {
    int adId = ad.adId;
    if (ad.adId == 0) {
      this.adId += 1;
      adId = this.adId;
    }
    map[adId] = ad;
    return adId;
  }

  void unregist(ADSuyiAd ad) {
    adId = ad.adId;
    map.remove(adId);
  }

  int adIdFor(ADSuyiAd ad) {
    return ad.adId;
  }

  ADSuyiAd adFor(int adId) {
    return map[adId];
  }
}

class ADSuyiWidget extends StatelessWidget {
  ADSuyiWidget({Key key, this.adView}) : super(key: key);

  final ADSuyiAdView adView;

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return Container(
          width: this.adView.width,
          height: this.adView.height,
          child: UiKitView(
              viewType: "ADSuyiAdView",
              creationParams: instanceManager.adIdFor(this.adView),
              creationParamsCodec: StandardMessageCodec()));
    } else if (Platform.isAndroid) {
      return Container(
          width: this.adView.width,
          height: this.adView.height,
          child: AndroidView(
              viewType: "ADSuyiAdView",
              creationParams: instanceManager.adIdFor(this.adView),
              creationParamsCodec: StandardMessageCodec()));
    }
    return Container();
  }
}