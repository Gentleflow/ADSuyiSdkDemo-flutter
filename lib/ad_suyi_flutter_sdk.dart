import 'dart:async';
import 'package:ad_suyi_flutter_sdk/ad_suyi_ad.dart';
import 'package:ad_suyi_flutter_sdk/ad_suyi_base.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';

class AdSuyiFlutterSdk {
  static const MethodChannel _channel = const MethodChannel('ad_suyi_sdk');

  static void _onAdEvent(
      ADSuyiAd ad, String eventName, Map<dynamic, dynamic> arguments) {
    switch (eventName) {
      case 'onSucced':
        if (ad.onSucced != null) {
          ad.onSucced();
        }
        break;
      case 'onFailed':
        if (ad.onFailed != null) {
          ad.onFailed();
        }
        break;
      case 'onExposed':
        if (ad.onExposed != null) {
          ad.onExposed();
        }
        break;
      case 'onClicked':
        if (ad.onClicked != null) {
          ad.onClicked();
        }
        break;
      case 'onClosed':
        if (ad.onClosed != null) {
          ad.onClosed();
        }
        break;
      case 'onRewarded':
        if (ad.onRewarded != null) {
          ad.onRewarded();
        }
        break;
      case 'onReceived':
        final int adViewId = arguments['adViewId'];
        final double width = arguments['adWidth'];
        final double height = arguments['adHeight'];
        ADSuyiFlutterNativeAd nativeAd = ad;
        nativeAd.onReceivedHandle(adViewId, width, height);
        break;
    }
  }

  static void setupMethodCallHandler() {
    _channel.setMethodCallHandler((MethodCall call) async {
      final int adId = call.arguments['adId'];
      ADSuyiAd ad = instanceManager.adFor(adId);
      _onAdEvent(ad, call.method, call.arguments);
    });
  }

  static Future<void> initSdk({@required String appid}) async {
    setupMethodCallHandler();
    assert(appid != null);
    print(appid);
    final void result =
        await _channel.invokeMethod('initSdk', {"appid": appid});
    return result;
  }

  static Future<void> releaseAd({@required int adId}) async {
    final void result = await _channel.invokeMethod('releaseAd', adId);
    return result;
  }

  static Future<void> loadSplashAd({
    @required String posId,
    @required int adId,
    @required String imgName,
  }) async {
    assert(posId != null);
    final void result = await _channel.invokeMethod('loadSplashAd', {
      "posId": posId,
      "adId": adId,
      "imgName": imgName,
    });
    return result;
  }

  static Future<void> loadRewardAd({
    @required String posId,
    @required int adId,
  }) async {
    assert(posId != null);
    final void result = await _channel.invokeMethod('loadRewardAd', {
      "posId": posId,
      "adId": adId,
    });
    return result;
  }

  static Future<void> showRewardAd({
    @required int adId,
  }) async {
    final void result = await _channel.invokeMethod('showRewardAd', {
      "adId": adId,
    });
    return result;
  }

  static Future<void> loadIntertitialAd({
    @required String posId,
    @required int adId,
  }) async {
    assert(posId != null);
    final void result = await _channel.invokeMethod('loadIntertitialAd', {
      "posId": posId,
      "adId": adId,
    });
    return result;
  }

  static Future<void> showIntertitialAd({
    @required int adId,
  }) async {
    final void result = await _channel.invokeMethod('showIntertitialAd', {
      "adId": adId,
    });
    return result;
  }

  static Future<void> loadFullscreenVodAd({
    @required String posId,
    @required int adId,
  }) async {
    assert(posId != null);
    final void result = await _channel.invokeMethod('loadFullScreenVodAd', {
      "posId": posId,
      "adId": adId,
    });
    return result;
  }

  static Future<void> showFullscreenVodAd({
    @required int adId,
  }) async {
    final void result = await _channel.invokeMethod('showFullScreenVodAd', {
      "adId": adId,
    });
    return result;
  }

  static Future<void> loadBannerAd({
    @required String posId,
    @required int adId,
    @required double adWidth,
    @required double adHeight,
  }) async {
    assert(posId != null);
    final void result = await _channel.invokeMethod('loadBannerAd', {
      "adId": adId,
      "adWidth": adWidth,
      "adHeight": adHeight,
      "posId": posId,
    });
    return result;
  }

  static Future<void> loadNativeAd({
    @required String posId,
    @required double width,
    @required int adId,
  }) async {
    assert(posId != null);
    final void result = await _channel.invokeMethod(
        'loadNativeAd', {"posId": posId, "adWidth": width, "adId": adId});
    return result;
  }
}
