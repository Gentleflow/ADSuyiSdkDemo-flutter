import 'package:ad_suyi_flutter_sdk/ad_suyi_ad.dart';
import 'package:ad_suyi_flutter_sdk/ad_suyi_base.dart';
import 'package:flutter/material.dart';

import 'key.dart';

class BannerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BannerState();
}

class _BannerState extends State<BannerPage> {
  ADSuyiFlutterBannerAd _adSuyiFlutterBannerAd;
  bool _hasInitBanner = false;

  @override
  Widget build(BuildContext context) {
    if (_adSuyiFlutterBannerAd == null && _hasInitBanner == false) {
      MediaQueryData queryData = MediaQuery.of(context);
      _hasInitBanner = true;
      var width = queryData.size.width;
      var height = queryData.size.width / 320.0 * 50.0;
      _adSuyiFlutterBannerAd = ADSuyiFlutterBannerAd(posId: KeyManager.bannerPosid(), width: width, height: height);
      _adSuyiFlutterBannerAd.loadAndShow();
      _adSuyiFlutterBannerAd.onSucced = () {
        print("横幅广告加载成功");
      };
      _adSuyiFlutterBannerAd.onFailed = () {
        removeBannerAd();
        print("横幅广告加载失败");
      };
      _adSuyiFlutterBannerAd.onClicked = () {
        print("横幅广告点击");
      };
      _adSuyiFlutterBannerAd.onExposed = () {
        print("横幅广告渲染成功");
      };
      _adSuyiFlutterBannerAd.onClosed = () {
        removeBannerAd();
        print("横幅广告关闭成功");
      };
    }

    return Scaffold(
        appBar: AppBar(
          title: Text("BannerPage"),
        ),
        body: Center(child: (_adSuyiFlutterBannerAd == null ? Text("banner广告已关闭") : ADSuyiWidget(adView: _adSuyiFlutterBannerAd) ))
    );
  }

  void removeBannerAd() {
    setState(() {
      releaseBannerAd();
    });
  }

  void releaseBannerAd() {
    _adSuyiFlutterBannerAd?.release();
    _adSuyiFlutterBannerAd = null;
  }

  @override
  void dispose() {
    releaseBannerAd();
    super.dispose();
  }
}
