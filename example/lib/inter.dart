import 'package:flutter/material.dart';
import 'package:ad_suyi_flutter_sdk/ad_suyi_ad.dart';

import 'key.dart';

class InterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _InterState();
}
class _InterState extends State<InterPage> {

  ADSuyiIntertitialAd _interAd;

  @override
  Widget build(BuildContext context) {
    showInterAd();
    return Scaffold(
      appBar: AppBar(
        title: Text("Intertitial"),
      ),
      body: Center(
      ),
    );
  }
  // 插屏
  // 显示插屏广告请保证当时app内没有其他地方显示插屏广告，否则会有冲突
  void showInterAd() {
    if(_interAd != null) {
      return;
    }
    _interAd = ADSuyiIntertitialAd(posId: KeyManager.interPosid());
    _interAd.onClicked = () {
      print("插屏广告关闭了");
    };
    _interAd.onFailed = () {
      print("插屏广告失败了");
      releaseInterAd();
    };
    _interAd.onExposed = () {
      print("插屏广告曝光了");
    };
    _interAd.onSucced = () {
      print("插屏广告成功了");
      playInterAd();
    };
    _interAd.onClicked = () {
      print("插屏广告点击了");
    };
    _interAd.onRewarded = () {
      print("插屏广告激励达成");
    };
    _interAd.onClosed = () {
      print("插屏广告关闭");
      releaseInterAd();
    };
    _interAd.load();
  }

  void releaseInterAd() {
    _interAd?.release();
    _interAd = null;
  }

  void playInterAd() {
    _interAd.show();
  }

  @override
  void dispose() {
    releaseInterAd();
    super.dispose();
  }

}