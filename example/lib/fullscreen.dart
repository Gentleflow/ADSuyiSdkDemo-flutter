import 'package:flutter/material.dart';
import 'package:ad_suyi_flutter_sdk/ad_suyi_ad.dart';

import 'key.dart';

class FullScreenPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => FullScreenState();

}
class FullScreenState extends State<FullScreenPage> {

  ADSuyiFullScreenVodAd _fullScreenVodAd;

  @override
  Widget build(BuildContext context) {
    showFullScreenVodAd();
    return Scaffold(
      appBar: AppBar(
        title: Text("FullScreenVodAd"),
      ),
      body: Center(
      ),
    );
  }
  // 全屏视频
  // 显示全屏视频广告请保证当时app内没有其他地方显示全屏视频广告，否则会有冲突
  void showFullScreenVodAd() {
    if(_fullScreenVodAd != null) {
      return;
    }
    _fullScreenVodAd = ADSuyiFullScreenVodAd(posId: KeyManager.fullScreenPosid());
    _fullScreenVodAd.onClicked = () {
      print("全屏视频广告关闭了");
    };
    _fullScreenVodAd.onFailed = () {
      print("全屏视频广告失败了");
      releaseFullScreenVodAd();
    };
    _fullScreenVodAd.onExposed = () {
      print("全屏视频广告曝光了");
    };
    _fullScreenVodAd.onSucced = () {
      print("全屏视频广告成功了");
      playFullScreenVodAd();
    };
    _fullScreenVodAd.onClicked = () {
      print("全屏视频广告点击了");
    };
    _fullScreenVodAd.onRewarded = () {
      print("全屏视频广告激励达成");
    };
    _fullScreenVodAd.onClosed = () {
      print("全屏视频广告关闭");
      releaseFullScreenVodAd();
    };
    _fullScreenVodAd.load();
  }

  void releaseFullScreenVodAd() {
    _fullScreenVodAd?.release();
    _fullScreenVodAd = null;
  }

  void playFullScreenVodAd() {
    _fullScreenVodAd.show();
  }

  @override
  void dispose() {
    releaseFullScreenVodAd();
    super.dispose();
  }

}