#import "AdSuyiFlutterSdkPlugin.h"
#if __has_include(<ad_suyi_flutter_sdk/ad_suyi_flutter_sdk-Swift.h>)
#import <ad_suyi_flutter_sdk/ad_suyi_flutter_sdk-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "ad_suyi_flutter_sdk-Swift.h"
#endif


@implementation AdSuyiFlutterSdkPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    [SwiftAdSuyiFlutterSdkPlugin registerWithRegistrar:registrar];
}
@end
